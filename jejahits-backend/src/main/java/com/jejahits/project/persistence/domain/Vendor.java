package com.jejahits.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "VENDOR")
@DynamicUpdate
@Data
public class Vendor extends Base {
    @Column(name = "VENDOR_NAME", nullable = false, length = 50)
    private String vendorName;

    @Column(name = "PHONE", nullable = false, length = 50)
    private String phone;

    @Column(name = "TANGGAL_LAHIR", nullable = false)
    private Date tanggalLahir;

    @Column(name = "JENIS_KELAMIN", nullable = false)
    private String jenisKelamin;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @ManyToOne
    @JoinColumn(name = "provinsi_id", nullable = false)
    private Provinsi provinsi;

    @ManyToOne
    @JoinColumn(name = "kabupaten_id", nullable = false)
    private Kabupaten kabupaten;

    @ManyToOne
    @JoinColumn(name = "kecamatan_id", nullable = false)
    private Kecamatan kecamatan;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "ALAMAT", nullable = false,length = 100)
    private String alamat;

    @Column(name = "ACTIVE")
    private Boolean active;

    @Column(name ="rating")
    private Integer rating;

    @PrePersist
    public void prePersist(){
        this.active = Boolean.TRUE;

    }
}
