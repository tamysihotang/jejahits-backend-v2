package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.util.List;

public interface VendorRepository extends BaseRepository<Vendor> {
    Page<Vendor> findByVendorNameLikeIgnoreCase (String vendorName, Pageable pageable);
}
