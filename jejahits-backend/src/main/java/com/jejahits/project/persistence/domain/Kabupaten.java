package com.jejahits.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Table(name = "KABUPATEN")
@DynamicUpdate
@Data
public class Kabupaten extends Base{
    @Column(name = "NAMA_KABUPATEN", nullable = false)
    private String namaKabupaten;

    @ManyToOne
    @JoinColumn(name = "provinsi")
    private Provinsi provinsi;
}
