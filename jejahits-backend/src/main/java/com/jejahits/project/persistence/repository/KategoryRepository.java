package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Kategory;
import org.springframework.stereotype.Repository;

@Repository
public interface KategoryRepository extends BaseRepository<Kategory> {
    Kategory findByKategoryName (String kategoryName);
}
