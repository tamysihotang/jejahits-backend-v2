package com.jejahits.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Created by Tamy 13 Okt 2019
 */

@Entity
@Table(name = "PROVINSI")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "project")
@DynamicUpdate
@Data
public class Provinsi extends Base{

    @Column(name = "NAMA_PROVINSI", nullable = false)
    private String namaProvinsi;

}
