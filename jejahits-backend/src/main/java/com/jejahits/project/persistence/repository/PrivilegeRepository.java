package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Privilege;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PrivilegeRepository extends BaseRepository<Privilege> {

    public Privilege findByName(String name);

    public List<Privilege> findByCategory(String category);

    public Privilege findBySecureId(String secureID);

    public Privilege findById(Integer id);

    public List<Privilege> findBySecureIdIn(List<String> id);

    public List<Privilege> findByIdNotIn(List<Integer> id);

}
