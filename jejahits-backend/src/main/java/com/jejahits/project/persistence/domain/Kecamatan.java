package com.jejahits.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Table(name = "KECAMATAN")
@DynamicUpdate
@Data
public class Kecamatan extends Base {
    @Column(name = "NAMA_KECAMATAN", nullable = false)
    private String namaKecamatan;

    @ManyToOne
    @JoinColumn(name = "kabupaten")
    private Kabupaten kabupaten;
}
