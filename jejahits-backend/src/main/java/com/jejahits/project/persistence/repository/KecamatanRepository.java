package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Kecamatan;

import java.util.List;


public interface KecamatanRepository extends BaseRepository<Kecamatan> {
    List<Kecamatan> findByKabupaten (Kabupaten kabupaten);
}
