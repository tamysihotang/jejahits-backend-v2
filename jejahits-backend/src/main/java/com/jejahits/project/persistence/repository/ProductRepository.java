package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Kategory;
import com.jejahits.project.persistence.domain.Product;
import com.jejahits.project.persistence.domain.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends BaseRepository<Product> {
    Page<Product> findByVendor (Vendor vendor, Pageable pageable);
}
