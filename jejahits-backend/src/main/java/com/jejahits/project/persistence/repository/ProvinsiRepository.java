package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Provinsi;
import org.springframework.stereotype.Repository;


@Repository
public interface ProvinsiRepository extends  BaseRepository<Provinsi>{
}
