package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.AccessLevel;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface AccessLevelRepository  extends BaseRepository<AccessLevel> {
    public AccessLevel findBySecureId(String secureId);
    public AccessLevel findByValue(String value);
    public List<AccessLevel> findByValueGreaterThanEqual(String value);
}
