package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Provinsi;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KabupatenRepository extends BaseRepository<Kabupaten>{
    List<Kabupaten> findByProvinsi (Provinsi provinsi);
}
