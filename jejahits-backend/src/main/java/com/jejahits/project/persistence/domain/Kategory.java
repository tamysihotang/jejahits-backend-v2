package com.jejahits.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Tamy 13 Okt 2019
 */

@Entity
@Table(name = "KATEGORY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "project")
@DynamicUpdate
@Data
public class Kategory extends Base{
    @Column(name = "KATEGORY_NAME", nullable = false)
    private String kategoryName;
}
