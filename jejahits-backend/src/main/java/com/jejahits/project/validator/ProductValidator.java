package com.jejahits.project.validator;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ProductValidator {

    public String checkMultipartFile(MultipartFile multipartFile) {
        String fileName = multipartFile.getOriginalFilename();
        String ext = FilenameUtils.getExtension(fileName);

        if (!ext.equalsIgnoreCase("jpg") && !ext.equalsIgnoreCase("jpeg")) {
            return "File format harus JPG/JPEG";
        }

        return null;
    }
}
