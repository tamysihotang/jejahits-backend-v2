package com.jejahits.project.oauth.provider.client;
	
import com.jejahits.project.oauth.provider.JejahitsClientDetailService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.provider.ClientDetails;


public class UserJejahitsClientDetailsService implements UserDetailsService {

	private JejahitsClientDetailService jejahitsClientDetailService;

	public UserJejahitsClientDetailsService(JejahitsClientDetailService jejahitsClientDetailService) {
		this.jejahitsClientDetailService = jejahitsClientDetailService;
	}

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		ClientDetails clientDetails = jejahitsClientDetailService.loadClientByClientId(username);
		String clientSecret = clientDetails.getClientSecret() == null ? "" : clientDetails.getClientSecret();
		return new User(username, clientSecret, clientDetails.getAuthorities());	}

}
