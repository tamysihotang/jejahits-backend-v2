package com.jejahits.project.oauth.provider.token;

import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

/**
 * agus w
 */
public class JejahitsTokenServices extends DefaultTokenServices {

    @Override
    protected boolean isSupportRefreshToken(OAuth2Request clientAuth) {
        return false;
    }

}
