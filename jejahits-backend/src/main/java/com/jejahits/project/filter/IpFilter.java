package com.jejahits.project.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.io.IOException;
import java.util.List;

/**
 * agus w on 2/29/16.
 */
public class IpFilter implements Filter {

    public static final Logger logger = LoggerFactory.getLogger(IpFilter.class);

    private List<String> allowedIp;

    public IpFilter(List<String> allowedIp) {
        this.allowedIp = allowedIp;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        if(!allowedIp.contains(servletRequest.getRemoteAddr())){
            servletResponse.getWriter().write("an unauthorized host list" +allowedIp);
        }else {
            filterChain.doFilter(servletRequest, servletResponse);
        }

    }

    @Override
    public void destroy() {

    }
}
