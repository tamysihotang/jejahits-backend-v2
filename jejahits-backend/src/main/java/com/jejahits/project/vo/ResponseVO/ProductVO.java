package com.jejahits.project.vo.ResponseVO;

import com.jejahits.project.vo.BaseVO;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductVO extends BaseVO {

    private String kategory;
    private String vendor;
    private BigDecimal hargaProduct;
    private String serviceType;
}
