package com.jejahits.project.vo.ResponseVO;

import com.jejahits.project.vo.BaseVO;
import lombok.Data;

import java.util.Date;

@Data
public class CustomerVO extends BaseVO {
    private String customerName;

    private String phone;

    private Date tanggalLahir;

    private String jenisKelamin;

    private String email;

    private String provinsi;

    private String kabupaten;

    private String kecamatan;

    private String username;

    private String alamat;

    private Boolean active;
}
