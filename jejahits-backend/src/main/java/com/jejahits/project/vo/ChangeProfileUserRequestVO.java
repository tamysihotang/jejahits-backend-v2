package com.jejahits.project.vo;

import lombok.Data;


@Data
public class ChangeProfileUserRequestVO {

    private String userId;
    private String fullName;
    private String email;
    private Boolean active;
}
