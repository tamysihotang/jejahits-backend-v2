package com.jejahits.project.vo.RequestVO;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductRequestVO {
    private String kategoryId;
    private BigDecimal hargaProduct;
    private String serviceType;

}
