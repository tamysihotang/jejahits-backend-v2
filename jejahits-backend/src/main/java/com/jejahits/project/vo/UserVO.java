package com.jejahits.project.vo;
import lombok.Data;

import java.util.Date;


@Data
public class UserVO extends BaseVO {
    private String email;
    private Boolean active;
    private String username ;
    private Boolean immediateChangePassword;
    private Date lastUpdatedDate;

}
