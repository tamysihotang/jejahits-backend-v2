package com.jejahits.project.vo;

import lombok.Data;

@Data
public class RegistrationClientRequestVO {

    private String email;
    private String username;
    private String password;
    private String confirmationPassword;

}
