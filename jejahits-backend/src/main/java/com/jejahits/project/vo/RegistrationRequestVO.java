package com.jejahits.project.vo;

import lombok.Data;


@Data
public class RegistrationRequestVO extends RegistrationUserRequestVO{

    private String phoneNumber;
}
