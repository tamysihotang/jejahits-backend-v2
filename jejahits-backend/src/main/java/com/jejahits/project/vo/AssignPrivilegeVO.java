package com.jejahits.project.vo;

import lombok.Data;

import java.util.List;


@Data
public class AssignPrivilegeVO {
   private String groupId;
   private String newName;
   private List<BaseVO> privilegeList;

}
