package com.jejahits.project.vo;

import lombok.Data;


@Data
public class PrivilegeVO extends BaseVO {
    private String name;
    private String description;
    private Boolean value;
}
