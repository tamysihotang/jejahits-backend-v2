package com.jejahits.project.vo;

import lombok.Data;


@Data
public class SignInRequestVO {

//    private String email;
    private String username;
    private String password;
}
