package com.jejahits.project.vo.ResponseVO;

import com.jejahits.project.vo.BaseVO;
import lombok.Data;

@Data
public class KategoryVO extends BaseVO {
    private String kategoryName;
}
