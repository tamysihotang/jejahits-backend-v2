package com.jejahits.project.vo.ResponseVO;

import com.jejahits.project.persistence.domain.Kecamatan;
import com.jejahits.project.vo.BaseVO;
import lombok.Data;

import java.util.List;

@Data
public class KabupatenVO extends BaseVO {
    private String namaKabupaten;
    List<KecamatanVO> kecamatanList;
}
