package com.jejahits.project.vo;

import lombok.Data;

import java.util.List;


@Data
public class NewAccessLevelVO {
    private List<AccessLevelVO> newAccessLevel;
}
