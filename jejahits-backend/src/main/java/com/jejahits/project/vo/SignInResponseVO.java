package com.jejahits.project.vo;

import lombok.Data;

@Data
public class SignInResponseVO {

    private String accessToken;

    private Boolean immediateChangePassword;

    private String fullName;

    private String domain;

}
