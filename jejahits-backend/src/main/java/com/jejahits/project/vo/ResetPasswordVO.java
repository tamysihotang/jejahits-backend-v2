package com.jejahits.project.vo;

import lombok.Data;


@Data
public class ResetPasswordVO {

    private String email;
}
