package com.jejahits.project.vo;

import lombok.Data;


@Data
public class AssignGroupVO {

    private Boolean force;
    private String userId;
    private String groupId;

}
