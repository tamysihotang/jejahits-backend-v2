package com.jejahits.project.vo;

import lombok.Data;

import java.util.List;


@Data
public class PrivilegeListVO {
    private String category;
    private Boolean value;
    private List<PrivilegeVO> privilegeVOList;
}
