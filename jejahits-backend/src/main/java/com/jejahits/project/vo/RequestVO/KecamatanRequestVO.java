package com.jejahits.project.vo.RequestVO;

import com.jejahits.project.vo.ResponseVO.KabupatenVO;
import lombok.Data;

@Data
public class KecamatanRequestVO {
    private String name;
    private KabupatenVO kabupaten;
}
