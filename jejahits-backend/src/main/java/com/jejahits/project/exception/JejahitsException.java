package com.jejahits.project.exception;

import com.jejahits.project.util.StatusCode;

public class JejahitsException extends RuntimeException {

    private StatusCode code = StatusCode.ERROR;

	public JejahitsException(){
		super();
	}

    public JejahitsException(String message){
		super(message);
	}

    public JejahitsException(StatusCode code, String message) {
        super(message);
        this.code = code;
    }

    public StatusCode getCode() {
        return code;
    }

    public void setCode(StatusCode code) {
        this.code = code;
    }

}
