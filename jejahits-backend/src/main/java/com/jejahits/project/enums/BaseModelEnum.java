package com.jejahits.project.enums;


/**
 * Base class untuk enum
 * <T> tipe dari internal valuenya
 * @author <a href="mailto:sihotangtamy@gmail.com">tamy</a>
 */
public interface BaseModelEnum<T> {
	
	/**
	 * get internal value
	 */
	T getInternalValue();
}
