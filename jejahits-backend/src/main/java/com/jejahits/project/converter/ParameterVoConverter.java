package com.jejahits.project.converter;

import com.jejahits.project.persistence.domain.Parameter;
import com.jejahits.project.vo.ParameterVO;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import org.springframework.stereotype.Component;

/**
 * Created by PanjiAlam on 8/29/2016.
 */
@Component
public class ParameterVoConverter extends BaseVoConverter<ParameterVO, ParameterVO, Parameter> implements IBaseVoConverter<ParameterVO,ParameterVO, Parameter>{
    @Override
    public ParameterVO transferModelToVO(Parameter model, ParameterVO vo){
        if(null == vo) vo = new ParameterVO();
        super.transferModelToVO(model, vo);

        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"code", "value", "description"});
        return vo;
    }
}