package com.jejahits.project.converter;

import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Provinsi;
import com.jejahits.project.persistence.repository.KabupatenRepository;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import com.jejahits.project.vo.ResponseVO.KabupatenVO;
import com.jejahits.project.vo.ResponseVO.ProvinsiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProvinsiConverter extends BaseVoConverter<ProvinsiVO, ProvinsiVO, Provinsi> implements IBaseVoConverter<ProvinsiVO,ProvinsiVO, Provinsi>{
    @Autowired
    KabupatenRepository kabupatenRepository;

    @Autowired
    KabupatenVOConverter kabupatenVOConverter;

    @Override
    public ProvinsiVO transferModelToVO(Provinsi model, ProvinsiVO vo){
        if(null == vo) vo = new ProvinsiVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"namaProvinsi"});
        List<KabupatenVO> kabupatenVOList = new ArrayList<>();
        List<Kabupaten> kabupatenList = kabupatenRepository.findByProvinsi(model);
        kabupatenVOConverter.transferListOfModelToListOfVO(kabupatenList,kabupatenVOList);
        vo.setKabupatenVOList(kabupatenVOList);
        return vo;
    }
}
