package com.jejahits.project.converter;

import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Kategory;
import com.jejahits.project.persistence.domain.Product;
import com.jejahits.project.persistence.domain.Vendor;
import com.jejahits.project.persistence.repository.KategoryRepository;
import com.jejahits.project.persistence.repository.VendorRepository;
import com.jejahits.project.util.Constants;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import com.jejahits.project.vo.RequestVO.ProductRequestVO;
import com.jejahits.project.vo.ResponseVO.ProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductVOConverter extends BaseVoConverter<ProductVO, ProductVO, Product> implements IBaseVoConverter<ProductVO,ProductVO, Product> {
    @Autowired
    KategoryRepository kategoryRepository;

    @Autowired
    VendorRepository vendorRepository;

    @Override
    public Product transferVOToModel(ProductVO vo, Product model) {
        if (null == model) model = new Product();

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"kategory","vendor","hargaProduct","serviceType"});
        if(vo.getId() != null)model.setSecureId(vo.getId());
        return model;
    }


    @Override
    public ProductVO transferModelToVO(Product model, ProductVO vo) {

        if (null == vo) vo = new ProductVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"kategory","vendor","hargaProduct","serviceType"});
        return vo;
    }

    public Product transferVOToModel(ProductRequestVO vo, Product model) {
        if (null == model) model = new Product();

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"kategory","vendor","hargaProduct","serviceType"});
        Kategory kategory = kategoryRepository.findBySecureId(vo.getKategoryId());
        if(kategory == null){
            throw new JejahitsException("Kategory Not Found");
        }
        model.setKategory(kategory);
        model.setServiceType(vo.getServiceType().toUpperCase());
        return model;
    }
}
