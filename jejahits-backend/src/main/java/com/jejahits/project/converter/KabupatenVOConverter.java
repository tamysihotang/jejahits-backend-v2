package com.jejahits.project.converter;

import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Kecamatan;
import com.jejahits.project.persistence.repository.KecamatanRepository;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import com.jejahits.project.vo.ResponseVO.KabupatenVO;
import com.jejahits.project.vo.ResponseVO.KecamatanVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class KabupatenVOConverter extends BaseVoConverter<KabupatenVO, KabupatenVO, Kabupaten> implements IBaseVoConverter<KabupatenVO,KabupatenVO, Kabupaten>{
    @Autowired
    KecamatanRepository kecamatanRepository;

    @Autowired
    KecamatanVOConverter kecamatanVOConverter;

    @Override
    public KabupatenVO transferModelToVO(Kabupaten model, KabupatenVO vo){
        if(null == vo) vo = new KabupatenVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"namaKabupaten"});
        List<KecamatanVO> kecamatanVOList = new ArrayList<>();
        List<Kecamatan> kabupatenList = kecamatanRepository.findByKabupaten(model);
        kecamatanVOConverter.transferListOfModelToListOfVO(kabupatenList,kecamatanVOList);
        vo.setKecamatanList(kecamatanVOList);
        return vo;
    }
}
