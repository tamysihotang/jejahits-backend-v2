package com.jejahits.project.web;

import com.jejahits.project.service.ProductService;
import com.jejahits.project.vo.RequestVO.KategoryRequestVO;
import com.jejahits.project.vo.RequestVO.ProductRequestVO;
import com.jejahits.project.vo.ResultPageVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @ApiOperation(value = "Create Product")
    @RequestMapping(value = "/create-product",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> create(@RequestBody List<ProductRequestVO> productRequestVO,
                                           @RequestParam(value = "vendorId", required = true)  String vendorId) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return productService.createProduct(productRequestVO, vendorId);
            }
        };
        return handler.getResult();
    }

    @ApiOperation(value = "Katalog Toko Saya")
    @RequestMapping(value = "/list-of-product",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultPageVO> getListOfCustomer(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                                          @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                          @RequestParam(value = "vendorId", required = false, defaultValue = "") String vendorId) {
        Map<String, Object> pageMap = productService.listProductByVendor(page,limit,vendorId);
        return constructListResult(pageMap);
    }

    protected ResponseEntity<ResultPageVO> constructListResult(Map<String, Object> pageMap) {
        return AbstractRequestHandler.constructListResult(pageMap);
    }

    @ApiOperation(value = "Upload")
    @RequestMapping(
            value = "/upload",
            method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> upload(@RequestParam MultipartFile file) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return productService.uploadFoto(file);
            }
        };
        return handler.getResult();
    }
}
