package com.jejahits.project.web;

import com.jejahits.project.vo.PrivilegeVO;
import com.jejahits.project.vo.ResultVO;
import com.jejahits.project.service.PrivilegeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/privilege")
public class PrivilegeController {

    public static final Logger logger = LoggerFactory.getLogger(PrivilegeController.class);

    @Autowired
    PrivilegeService privilegeService;

//    @PreAuthorize("isAuthenticated()")
//    @RequestMapping(value = "/list-of-privilege",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public ResponseEntity<ResultVO> getListOfPrivilege() {
//        AbstractRequestHandler handler = new AbstractRequestHandler() {
//            @Override
//            @Override
//            public Object processRequest() {
//                return privilegeService.getListOfPrivilege();
//            }
//        };
//        return handler.getResult();
//    }

}
