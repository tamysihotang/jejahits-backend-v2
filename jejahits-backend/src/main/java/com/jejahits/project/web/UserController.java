package com.jejahits.project.web;

import com.jejahits.project.vo.DeleteUserRequestVO;
import com.jejahits.project.vo.UpdateUserRequestVO;
import com.jejahits.project.service.BaseVoService;
import com.jejahits.project.persistence.domain.User;
import com.jejahits.project.service.UsersService;
import com.jejahits.project.vo.ResultPageVO;
import com.jejahits.project.vo.UserVO;
import com.jejahits.project.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/user")
public class UserController extends BaseController<User, UserVO, UserVO> {

    @Autowired
    UsersService userService;



    @Override
    protected BaseVoService<User, UserVO, UserVO> getDomainService() {
        return null;
    }

}
