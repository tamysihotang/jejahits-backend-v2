package com.jejahits.project.web;

import com.jejahits.project.service.SignInService;
import com.jejahits.project.vo.ResultVO;
import com.jejahits.project.vo.SignInRequestVO;
import com.jejahits.project.vo.SignOutRequestVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    @Autowired
    SignInService signInService;

    @PreAuthorize("permitAll")
    @RequestMapping(value = "/logout",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> logout(
            @RequestBody SignOutRequestVO signOutRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return signInService.logout(signOutRequestVO);
            }
        };
        return handler.getResult();
    }

    @ApiOperation(value = "Login")
    @PreAuthorize("permitAll")
    @RequestMapping(value = "/login",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> login(@RequestBody SignInRequestVO signInRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return signInService.loginUser(signInRequestVO);
            }
        };
        return handler.getResult();
    }
}
