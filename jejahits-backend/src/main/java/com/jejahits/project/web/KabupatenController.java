package com.jejahits.project.web;

import com.jejahits.project.service.KabupatenService;
import com.jejahits.project.vo.RequestVO.KabupatenRequestVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/kabupaten")
public class KabupatenController {
    @Autowired
    KabupatenService kabupatenService;

    @ApiOperation(value = "Create Kabupaten")
    @RequestMapping(value = "/create-kabupaten",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> create(@RequestBody KabupatenRequestVO kabupatenRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return kabupatenService.createKabupaten(kabupatenRequestVO);
            }
        };
        return handler.getResult();
    }
}
