package com.jejahits.project.web;

import com.jejahits.project.service.KecamatanService;
import com.jejahits.project.vo.RequestVO.KabupatenRequestVO;
import com.jejahits.project.vo.RequestVO.KecamatanRequestVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/kecamatan")
public class KecamatanController {
    @Autowired
    KecamatanService kecamatanService;

    @ApiOperation(value = "Create Kecamatan")
    @RequestMapping(value = "/create-kecamatan",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> create(@RequestBody KecamatanRequestVO kecamatanRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return kecamatanService.createKabupaten(kecamatanRequestVO);
            }
        };
        return handler.getResult();
    }
}
