package com.jejahits.project.web;

import com.jejahits.project.vo.AssignGroupVO;
import com.jejahits.project.vo.ChangePasswordRequestVO;
import com.jejahits.project.vo.ChangeProfileRequestVO;
import com.jejahits.project.vo.ChangeProfileUserRequestVO;
import com.jejahits.project.vo.*;
import com.jejahits.project.service.ProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;



@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/profiles")
public class ProfileController{

    public static final Logger logger = LoggerFactory.getLogger(ProfileController.class);

    @Autowired
    ProfileService profileService;

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/user",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getUser() {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return profileService.getUserProfile();
            }
        };
        return handler.getResult();
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/user/change-password",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> changePasswordUser(@RequestBody ChangePasswordRequestVO changePasswordRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return profileService.changePasswordUser(changePasswordRequestVO);
            }
        };
        return handler.getResult();
    }

}
