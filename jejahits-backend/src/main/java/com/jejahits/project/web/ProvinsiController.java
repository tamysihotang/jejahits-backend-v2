package com.jejahits.project.web;

import com.jejahits.project.service.ProvinsiService;
import com.jejahits.project.vo.RequestVO.ProvinsiRequestVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/provinsi")
public class ProvinsiController {
    @Autowired
    ProvinsiService provinsiService;

    @ApiOperation(value = "Create Provinsi")
    @RequestMapping(value = "/create-provinsi",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> create(@RequestBody ProvinsiRequestVO provinsiRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return provinsiService.createProvinsi(provinsiRequestVO);
            }
        };
        return handler.getResult();
    }

    @ApiOperation(value = "List Provinsi")
    @RequestMapping(value = "/get-list-provinsi",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> get() {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return provinsiService.listProvinsi();
            }
        };
        return handler.getResult();
    }
}
