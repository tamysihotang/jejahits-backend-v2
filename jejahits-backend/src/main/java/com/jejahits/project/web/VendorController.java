package com.jejahits.project.web;

import com.jejahits.project.service.VendorService;
import com.jejahits.project.vo.RequestVO.UpdateRatingVO;
import com.jejahits.project.vo.ResponseVO.VendorVO;
import com.jejahits.project.vo.ResultPageVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/vendor")
public class VendorController {
    @Autowired
    VendorService vendorService;
    //    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Search Vendor")
    @RequestMapping(value = "/list-of-vendor",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultPageVO> getListOfCustomer(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                                          @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                          @RequestParam(value = "vendorName", required = false, defaultValue = "") String vendorName) {
        Map<String, Object> pageMap = vendorService.listVendor(page,limit,vendorName);
        return constructListResult(pageMap);
    }

    protected ResponseEntity<ResultPageVO> constructListResult(Map<String, Object> pageMap) {
        return AbstractRequestHandler.constructListResult(pageMap);
    }

//    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get profile vendor")
    @RequestMapping(value = "/get-detail-vendor/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> retrieveData(@PathVariable String id) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return vendorService.getDetailVendor(id);
            }
        };
        return handler.getResult();
    }

    //    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Update Rating vendor")
    @RequestMapping(value = "/update-rating",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> updateRating(@RequestBody UpdateRatingVO updateRatingVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return vendorService.updateRatingVendor(updateRatingVO);
            }
        };
        return handler.getResult();
    }

    @ApiOperation(value = "Update Profile vendor")
    @RequestMapping(value = "/update",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> updateProfile(@RequestBody VendorVO vendorVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return vendorService.updateVendor(vendorVO);
            }
        };
        return handler.getResult();
    }

}
