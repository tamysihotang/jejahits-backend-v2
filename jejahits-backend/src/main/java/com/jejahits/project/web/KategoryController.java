package com.jejahits.project.web;

import com.jejahits.project.service.KategoryService;
import com.jejahits.project.vo.RequestVO.KategoryRequestVO;
import com.jejahits.project.vo.RequestVO.KecamatanRequestVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/kategory")
public class KategoryController {

    @Autowired
    KategoryService kategoryService;

    @ApiOperation(value = "Create Kategory")
    @RequestMapping(value = "/create-kategory",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> create(@RequestBody KategoryRequestVO kategoryRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return kategoryService.createKategory(kategoryRequestVO);
            }
        };
        return handler.getResult();
    }

    @ApiOperation(value = "List Kategory")
    @RequestMapping(value = "/get-list-kategory",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> get(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                        @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                        @RequestParam(value = "name", required = false, defaultValue = "") String name) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return kategoryService.listKategory(page, limit, name);
            }
        };
        return handler.getResult();
    }
}
