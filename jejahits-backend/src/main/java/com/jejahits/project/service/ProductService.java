package com.jejahits.project.service;

import com.jejahits.project.converter.ProductVOConverter;
import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Kategory;
import com.jejahits.project.persistence.domain.Product;
import com.jejahits.project.persistence.domain.Vendor;
import com.jejahits.project.persistence.repository.KategoryRepository;
import com.jejahits.project.persistence.repository.ProductRepository;
import com.jejahits.project.persistence.repository.VendorRepository;
import com.jejahits.project.util.Constants;
import com.jejahits.project.util.DocumentUtils;
import com.jejahits.project.validator.ProductValidator;
import com.jejahits.project.vo.RequestVO.KategoryRequestVO;
import com.jejahits.project.vo.RequestVO.ProductRequestVO;
import com.jejahits.project.vo.ResponseVO.ProductVO;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductVOConverter productVOConverter;

    @Autowired
    VendorRepository vendorRepository;

    @Autowired
    ProductValidator productValidator;

    @Autowired
    DocumentUtils documentUtils;

    public Boolean createProduct(List<ProductRequestVO> productRequestVOList, String vendorId){

        Vendor vendor = vendorRepository.findBySecureId(vendorId);
        if(vendor==null){
            throw new JejahitsException("Vendor not found");
        }
        Product product = new Product();
        for(ProductRequestVO productRequestVO : productRequestVOList){
            productVOConverter.transferVOToModel(productRequestVO,product);
            product.setVendor(vendor);
            productRepository.save(product);
        }

        return Boolean.TRUE;
    }

    public Map<String, Object> listProductByVendor (Integer page, Integer limit, String vendorId){


        String sortBy = "Id";
        String direction = "desc";

        Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortBy, direction));
        Page<Product> resultPage =null;
        if(vendorId==""||vendorId==null){
            resultPage = productRepository.findAll(pageable);
        }else{
            Vendor vendor = vendorRepository.findBySecureId(vendorId);
            if(vendor==null){
                throw new JejahitsException("Vendor not Found");
            }
            resultPage = productRepository.findByVendor(vendor, pageable);
        }
        List<Product> models = resultPage.getContent();
        Collection<ProductVO> vos = productVOConverter.transferListOfModelToListOfVO(models, new ArrayList<>());
        return AbstractBaseVoService.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }

    @Transactional
    public String uploadFoto(MultipartFile multipartFile) {

        // get file
        String messageValidation = productValidator.checkMultipartFile(multipartFile);
        if (null != messageValidation) throw new JejahitsException(messageValidation);

        // save file
        String destination = "IMAGE";
//        String upload = documentUtils.uploadFile(multipartFile, destination);


//        if (upload.equalsIgnoreCase("ERROR")) {
//            throw new JejahitsException("Gagal mengupload foto");
//        }

            return "Upload berhasil";
        }

    }

