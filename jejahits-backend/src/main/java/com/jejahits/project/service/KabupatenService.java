package com.jejahits.project.service;

import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Provinsi;
import com.jejahits.project.persistence.repository.KabupatenRepository;
import com.jejahits.project.persistence.repository.ProvinsiRepository;
import com.jejahits.project.vo.RequestVO.KabupatenRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KabupatenService {
    @Autowired
    ProvinsiRepository provinsiRepository;

    @Autowired
    KabupatenRepository kabupatenRepository;

    public Boolean createKabupaten(KabupatenRequestVO kabupatenRequestVO){
        Kabupaten kabupaten = new Kabupaten();
        Provinsi provinsi = provinsiRepository.findBySecureId(kabupatenRequestVO.getProvinsi().getId());
        if(null==provinsi){
            throw new JejahitsException("Provinsi not found");
        }
        kabupaten.setNamaKabupaten(kabupatenRequestVO.getName());
        kabupaten.setProvinsi(provinsi);
        kabupatenRepository.save(kabupaten);
        return Boolean.TRUE;
    }
}
