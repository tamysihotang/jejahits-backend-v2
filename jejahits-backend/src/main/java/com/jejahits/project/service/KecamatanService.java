package com.jejahits.project.service;

import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Kecamatan;
import com.jejahits.project.persistence.repository.KabupatenRepository;
import com.jejahits.project.persistence.repository.KecamatanRepository;
import com.jejahits.project.vo.RequestVO.KecamatanRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KecamatanService {
    @Autowired
    KabupatenRepository kabupatenRepository;

    @Autowired
    KecamatanRepository kecamatanRepository;

    public Boolean createKabupaten(KecamatanRequestVO kecamatanRequestVO){
        Kecamatan kecamatan = new Kecamatan();
        Kabupaten kabupaten = kabupatenRepository.findBySecureId(kecamatanRequestVO.getKabupaten().getId());
        if(null==kabupaten){
            throw new JejahitsException("Kabupaten not found");
        }
        kecamatan.setNamaKecamatan(kecamatanRequestVO.getName());
        kecamatan.setKabupaten(kabupaten);
        kecamatanRepository.save(kecamatan);
        return Boolean.TRUE;
    }
}
