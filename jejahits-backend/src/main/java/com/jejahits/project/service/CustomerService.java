package com.jejahits.project.service;

import com.jejahits.project.converter.CustomerVOConverter;
import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Customer;
import com.jejahits.project.persistence.repository.CustomerRepository;
import com.jejahits.project.vo.ResponseVO.CustomerVO;
import com.jejahits.project.vo.ResponseVO.VendorVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerVOConverter customerVOConverter;

    public Map<String, Object> listCustomer(Integer page, Integer limit, String customerName) {
        String sortBy = "customerName";
        String direction = "desc";

        customerName = StringUtils.isEmpty(customerName) ? "%" : "%" + customerName + "%";
        Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortBy, direction));
        Page<Customer> resultPage = customerRepository.findByCustomerNameLikeIgnoreCase(customerName,pageable);

        List<Customer> models = resultPage.getContent();
        Collection<CustomerVO> vos = customerVOConverter.transferListOfModelToListOfVO(models, new ArrayList<>());
        return AbstractBaseVoService.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }

    public CustomerVO detailCustomer(String custId){
        Customer customer = customerRepository.findBySecureId(custId);
        if(null==customer){
            throw new JejahitsException("Customer not Found");
        }
        CustomerVO customerVO = customerVOConverter.transferModelToVO(customer,null);
        return customerVO;
    }

    public Boolean updateCustomer (CustomerVO customerVO){
        Customer customer = customerRepository.findBySecureId(customerVO.getId());
        if(null==customer){
            throw new JejahitsException("Customer not Found");
        }
        customer = customerVOConverter.transferVOToModel(customerVO,customer);
        customerRepository.saveAndFlush(customer);
        return Boolean.TRUE;
    }

}
