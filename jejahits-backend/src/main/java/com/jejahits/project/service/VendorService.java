package com.jejahits.project.service;

import com.jejahits.project.converter.VendorVOConverter;
import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Vendor;
import com.jejahits.project.persistence.repository.VendorRepository;
import com.jejahits.project.vo.RequestVO.UpdateRatingVO;
import com.jejahits.project.vo.ResponseVO.VendorVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service
public class VendorService {
    @Autowired
    VendorRepository vendorRepository;

    @Autowired
    VendorVOConverter vendorVOConverter;

    public Map<String, Object> listVendor(Integer page, Integer limit, String vendorName) {
        String sortBy = "vendorName";
        String direction = "desc";

        vendorName = StringUtils.isEmpty(vendorName) ? "%" : "%" + vendorName + "%";
        Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortBy, direction));
        Page<Vendor> resultPage = vendorRepository.findByVendorNameLikeIgnoreCase(vendorName,pageable);

        List<Vendor> models = resultPage.getContent();
        Collection<VendorVO> vos = vendorVOConverter.transferListOfModelToListOfVO(models, new ArrayList<>());
        return AbstractBaseVoService.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }

    public VendorVO getDetailVendor(String vendorId) {
     Vendor vendor = vendorRepository.findBySecureId(vendorId);
     if(vendor==null){
         throw new JejahitsException("Vendor not found");
     }
     VendorVO vendorVO = vendorVOConverter.transferModelToVO(vendor,null);
     return vendorVO;

    }

    public Boolean updateRatingVendor (UpdateRatingVO updateRatingVO){
        Vendor vendor = vendorRepository.findBySecureId(updateRatingVO.getId());
        if(vendor==null){
            throw new JejahitsException("Vendor not found");
        }
        vendor = vendorVOConverter.transferVOToModel(updateRatingVO,vendor);
        vendor.setRating(updateRatingVO.getRating());
        vendorRepository.save(vendor);
        return Boolean.TRUE;
    }

    public Boolean updateVendor (VendorVO vendorVO){
        Vendor vendor = vendorRepository.findBySecureId(vendorVO.getId());
        if(null == vendor){
            throw  new JejahitsException("Vendor not found");
        }
        vendor = vendorVOConverter.transferVOToModel(vendorVO,vendor);
        vendorRepository.saveAndFlush(vendor);
        return Boolean.TRUE;
    }
}
