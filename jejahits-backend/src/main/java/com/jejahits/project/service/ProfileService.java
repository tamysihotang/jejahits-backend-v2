package com.jejahits.project.service;

import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.*;
import com.jejahits.project.persistence.repository.*;
import com.jejahits.project.validator.ProfileValidator;
import com.jejahits.project.vo.*;
import com.jejahits.project.util.ErrorUtil;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * agus w
 */

@Service
public class ProfileService{

    public static final Logger logger = LoggerFactory.getLogger(ProfileService.class);

    @Autowired
    SignInService signInService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccessTokenRepository accessTokenRepository;

    @Autowired
    @Qualifier("passwordEncoder")
    PasswordEncoder passwordEncoder;

    @Autowired
    ProfileValidator profileValidator;

    @Value("${client.id.user}")
    private String clientIdUser;

    @Value("${client.id.admin}")
    private String clientIdAdmin;

    @Autowired
    PrivilegeService privilegeService;
    



    public Object changePasswordUser(ChangePasswordRequestVO changePasswordRequestVO) {

        String username = signInService.getUsername();

        User user = userRepository.findByUsername(username);

        if (null == user) throw new JejahitsException("Your profile not found");

        String messageValidation = profileValidator.validateChangePassword(changePasswordRequestVO, user.getPassword());

        if (null != messageValidation) throw new JejahitsException(messageValidation);

        user.setPassword(passwordEncoder.encode(changePasswordRequestVO.getNewPassword()));
        user.setImmediateChangePassword(false);
        user.setLastUpdatedPassword(new Date());
        userRepository.saveAndFlush(user);

        return passwordEncoder.matches(changePasswordRequestVO.getNewPassword(), user.getPassword());
    }

    public ProfileUserVO getUserProfile() {
        String username = signInService.getUsername();

        User admin = userRepository.findByUsername(username);

        if (null == admin) throw new JejahitsException("Your profile not found");

        ProfileVO profileVO = new ProfileVO();

        ExtendedSpringBeanUtil.copySpecificProperties(admin, profileVO,
                new String[]{"email", "fullName", "address"});

        return profileVO;
    }



}
