package com.jejahits.project.service;

import com.jejahits.project.converter.AccessLevelVoConverter;
import com.jejahits.project.converter.IBaseVoConverter;
import com.jejahits.project.persistence.domain.AccessLevel;
import com.jejahits.project.persistence.domain.AccessToken;
import com.jejahits.project.persistence.domain.User;
import com.jejahits.project.persistence.repository.*;
import com.jejahits.project.vo.AccessLevelVO;
import com.jejahits.project.vo.NewAccessLevelVO;
import com.jejahits.project.persistence.repository.*;
import com.jejahits.project.util.ErrorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PanjiAlam on 8/16/2016.
 */

@Service
public class AccessLevelService extends BaseVoService<AccessLevel, AccessLevelVO, AccessLevelVO>{

    @Autowired
    AccessLevelRepository acccessLevelRepository;

    @Autowired
    AccessLevelVoConverter accessLevelConverter;

    @Autowired
    SignInService signInService;

    @Autowired
    AccessTokenRepository accessTokenRepository;

    @Value("${client.id.hq}")
    String clientIdHq;

    @Value("${client.id.md}")
    String clientIdmd;

    @Value("${client.id.smk}")
    String clientIdsmk;


    @Override
    protected BaseRepository<AccessLevel> getJpaRepository() {
        return acccessLevelRepository;
    }

    @Override
    protected JpaSpecificationExecutor<AccessLevel> getSpecRepository() {
        return acccessLevelRepository;
    }

    @Override
    protected IBaseVoConverter<AccessLevelVO, AccessLevelVO, AccessLevel> getVoConverter() {
        return accessLevelConverter;
    }

    public AccessLevelVO getAccessLevel(String secureId){


        AccessLevel accessLevel = acccessLevelRepository.findBySecureId(secureId);

        AccessLevelVO accessLevelVO = new AccessLevelVO();
        getVoConverter().transferModelToVO(accessLevel,accessLevelVO);
        return accessLevelVO;

    }

    public List<AccessLevelVO> getListOfAccessLevel(){
        User user = signInService.getUser();
        if(user==null){
            throw ErrorUtil.notFoundError("User");
        }

        AccessToken token = accessTokenRepository.findByAccessToken(user.getAccessToken());

        String value="";

        if(token.getClientId().equals(clientIdHq)){
           value = "0";
        }
        else if(token.getClientId().equals(clientIdmd)){
//            value="1";
            value="2";
        }
        else if(token.getClientId().equals(clientIdsmk)){
            value="2";
        }

        List<AccessLevel> accessLevel = acccessLevelRepository.findByValueGreaterThanEqual(value);

        AccessLevelVO accessLevelVO = new AccessLevelVO();
        NewAccessLevelVO newAccessLevelVO = new NewAccessLevelVO();

        List<AccessLevelVO> accessLevelVOs = new ArrayList<>();
        accessLevelConverter.transferListOfModelToListOfVO(accessLevel, accessLevelVOs);
        newAccessLevelVO.setNewAccessLevel(accessLevelVOs);
        return accessLevelVOs;
    }

}