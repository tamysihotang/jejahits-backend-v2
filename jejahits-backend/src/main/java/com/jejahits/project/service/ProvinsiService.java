package com.jejahits.project.service;

import com.jejahits.project.converter.ProvinsiConverter;
import com.jejahits.project.persistence.domain.Provinsi;
import com.jejahits.project.persistence.repository.ProvinsiRepository;
import com.jejahits.project.vo.RequestVO.ProvinsiRequestVO;
import com.jejahits.project.vo.ResponseVO.ProvinsiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProvinsiService {

    @Autowired
    ProvinsiRepository provinsiRepository;

    @Autowired
    ProvinsiConverter provinsiConverter;

    public Boolean createProvinsi(ProvinsiRequestVO provinsiRequestVO){
        Provinsi provinsi = new Provinsi();
        provinsi.setNamaProvinsi(provinsiRequestVO.getName().toUpperCase());
        provinsiRepository.save(provinsi);
        return Boolean.TRUE;
    }

    public List<ProvinsiVO> listProvinsi(){
        List<ProvinsiVO> provinsiVOS = new ArrayList<>();
        List<Provinsi> provinsiList = provinsiRepository.findAll();
        for(Provinsi provinsi : provinsiList){
            ProvinsiVO provinsiVO = provinsiConverter.transferModelToVO(provinsi,null);
            provinsiVOS.add(provinsiVO);
        }
        return provinsiVOS;
    }
}
