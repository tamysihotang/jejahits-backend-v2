package com.jejahits.project.util;

import com.jejahits.project.persistence.domain.AccessToken;
import com.jejahits.project.persistence.domain.User;
import com.jejahits.project.persistence.repository.AccessTokenRepository;
import com.jejahits.project.service.SignInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class HierarchyUtil {

    @Autowired
    SignInService signInService;

    @Autowired
    AccessTokenRepository accessTokenRepository;

    @Value("${client.id.admin}")
    String clientIdAdmin;

    @Value("${client.id.user}")
    String clientIdUser;


    public Boolean isUser(){
        User user = signInService.getUser();
        AccessToken accessToken = accessTokenRepository.findByAccessToken(user.getAccessToken());
        if(accessToken.getClientId().equals(clientIdAdmin)) {
            return true;
        }
        return false;
    }

    public Boolean isAdmin(){
        User user = signInService.getUser();
        AccessToken accessToken = accessTokenRepository.findByAccessToken(user.getAccessToken());
        if(accessToken.getClientId().equals(clientIdUser)) {
            return true;
        }
        return false;
    }

}
