package com.jejahits.project.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * agus w on 3/7/16.
 */
public class DateUtils {

    final static String YYYYMMDD = "yyyy-MMM-dd";

    final static String DD_MMM_YYYY = "dd-MMM-yyyy";
    public final static String DD_MM_YYYY = "dd-MM-yyyy";
    public final static String YYYYMMDDHHMMSSSSS =  "yyyyMMddHHmmssSSS";

    public static String getToday(Date date) {
        return new SimpleDateFormat(YYYYMMDD).format(date);
    }
}
