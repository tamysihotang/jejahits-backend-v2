package com.jejahits.project.util;

public class Constants {

    public static class Multiplier {
        public static final Integer MINUS = 0;
        public static final Integer PLUS = 1;
    }


    public static final class RequestStatus {
        public static final String PENDING = "PENDING";
        public static final String APPROVED = "APPROVED";
        public static final String REJECTED = "REJECTED";
    }

    public static final class PageParameter {
        public static final String LIST_DATA = "listData";
        public static final String TOTAL_PAGES = "totalPages";
        public static final String TOTAL_ELEMENTS = "totalElements";
    }



    public static final class PrivilegeCategory{
        public static final String PRIVILEGE_USER = "Pengguna";
        public static final String PRIVILEGE_GROUP = "Grup";
        public static final String PRIVILEGE_REPORT = "Report";
    }



    public static final class NotificationKey{
        public static final String PASSWORD = "PASSWORD";
    }

    public static final class ParamCode{
        public static final String PASSWORD = "PASSWORD";
        public static final String LOGIN_EXPIRED = "LOGIN_EXPIRED";
    }
    public static final class TypeUser{
        public static final String CUSTOMER = "CUSTOMER";
        public static final String VENDOR = "VENDOR";
    }
    public static final class ServiceType{
        public static final String VERMAK = "VERMAK";
        public static final String JAHIT = "JAHIT";
    }


}
