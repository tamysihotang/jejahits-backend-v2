package com.jejahits.project.util;


import lombok.Data;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by derryaditiya on 9/11/16.
 */
@Component
@Data
public class ExcelUtil {

    private static final Logger logger = LoggerFactory.getLogger(ExcelUtil.class);

    final static String NUMBER_FORMAT = "#,##0";
    final static String NUMBER_FORMAT_DECIMAL = "#,##0.00";

    public final static String MEDIA_TYPE_XLS = "application/vnd.ms-excel";
    public final static String MEDIA_TYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    @Value(value = "${file.masterUrl}")
    String masterUrl;

    @Value("${file.pathExcel}")
    String pathExcelFile;

    @Value("${file.pathExcelForUrl}")
    String pathExcelUrl;

    public String generateExcel( String fileName, List<String> listTitle, List<String>listSheetName,
                                 List<List<Object[]>> listSearchParam, List<String[]> listHeader, List<List<Object[]>> listData ){

        String result = null;
        try {
            Workbook wb = new XSSFWorkbook();

            CellStyle headerStyle = headerStyle(wb);
            CellStyle textStyle = textStyle(wb);
            CellStyle numberStyle = numberStyle(wb);
            CellStyle dateStyle = dateStyle(wb);
            CellStyle textStyleBordered = textStyleBordered(wb);
            CellStyle numberStyleBordered = numberStyleBordered(wb);
            CellStyle dateStyleBordered = dateStyleBordered(wb);

            for(int i=0; i<listSheetName.size(); i++){
                Integer rowIdx = 0;

                Sheet sheet = null;
                if(listSheetName.size() > i && listSheetName.get(i) != null) {
                    sheet = wb.createSheet(listSheetName.get(i));
                }else{
                    sheet = wb.createSheet("Sheet "+i);
                }

                if(listTitle.size() > i && listTitle.get(i) != null) {
                    // sheet add sheetTitle
                    Row row = sheet.createRow(rowIdx);
                    row.createCell(0).setCellValue(listTitle.get(i));
                    rowIdx++;
                }


                // sheet add search param
                if(listSearchParam.size() > i) {
                    List<Object[]> paramSearchSheet = listSearchParam.get(i);
                    if (paramSearchSheet != null) {
                        for (int j = 0; j < paramSearchSheet.size(); j++) {
                            // loop row
                            rowIdx++;
                            Row nextRow = sheet.createRow(rowIdx);

                            Object[] paramSearch = paramSearchSheet.get(j);
                            for (int k = 0; k < paramSearch.length; k++) {
                                // loop column
                                Cell cell = nextRow.createCell(k);

                                if(paramSearch[k] == null){
                                    cell.setCellStyle(textStyle);

                                }else if(paramSearch[k] instanceof String){
                                    cell.setCellValue(paramSearch[k].toString());
                                    cell.setCellStyle(textStyle);

                                } else if (paramSearch[k] instanceof Number){
                                    cell.setCellValue(Double.parseDouble(paramSearch[k].toString()));
                                    cell.setCellStyle(numberStyle);

                                } else if (paramSearch[k] instanceof Date){
                                    Date date = org.apache.commons.lang3.time.DateUtils.truncate((Date) paramSearch[k], Calendar.DAY_OF_MONTH);
                                    cell.setCellValue(date);
                                    cell.setCellStyle(dateStyle);

                                }
                            }
                        }
                        rowIdx++;
                    }
                }

                // sheet add header
                if(listHeader.size() > i) {
                    String[] headerSheet = listHeader.get(i);
                    if (headerSheet != null) {
                        // loop header column
                        rowIdx++;
                        Row headerRow = sheet.createRow(rowIdx);
                        Cell cell = headerRow.createCell(0);
                        cell.setCellStyle(headerStyle);
                        cell.setCellValue("No");

                        for (int k = 1; k <= headerSheet.length; k++) {
                            // loop column
                            Cell nextCell = headerRow.createCell(k);
                            nextCell.setCellStyle(headerStyle);
                            nextCell.setCellValue(headerSheet[k-1]);

                        }
                    }
                }

                // sheet add data
                if(listData.size() > i) {
                    List<Object[]> dataSheet = listData.get(i);
                    if (dataSheet != null) {
                        for (int j = 0; j < dataSheet.size(); j++) {
                            // loop row
                            rowIdx++;
                            Row nextRow = sheet.createRow(rowIdx);

                            Object[] data = dataSheet.get(j);
                            Cell cellNo = nextRow.createCell(0);
                            cellNo.setCellValue(j+1);
                            cellNo.setCellStyle(headerStyle);

                            for (int k = 1; k <= data.length; k++) {
                                // loop column
                                Cell cell = nextRow.createCell(k);

                                if(data[k-1] == null){
                                    cell.setCellStyle(textStyleBordered);

                                }else if(data[k-1] instanceof String){
                                    cell.setCellValue(data[k-1].toString());
                                    cell.setCellStyle(textStyleBordered);

                                } else if (data[k-1] instanceof Number){
                                    cell.setCellValue(Integer.parseInt(data[k-1].toString()));
                                    cell.setCellStyle(numberStyleBordered);

                                } else if (data[k-1] instanceof Date){
                                    Date date = org.apache.commons.lang3.time.DateUtils.truncate((Date) data[k-1], Calendar.DAY_OF_MONTH);
                                    cell.setCellValue(date);
                                    cell.setCellStyle(dateStyleBordered);
                                }
                            }
                        }
                        rowIdx++;
                    }
                }

            }

            if(org.springframework.util.StringUtils.isEmpty(fileName)){
                fileName = "export-excel.xlsx";
            }


            String uri = pathExcelFile +fileName;
            FileOutputStream fileOut = new FileOutputStream(uri);
            wb.write(fileOut);
            fileOut.close();

            result = masterUrl+pathExcelUrl+fileName;

        } catch (Exception e) {
            e.printStackTrace();
            result = "ERROR";
        }

        return result;
    }

    private CellStyle textStyle(Workbook wb){
        CellStyle style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);

        return style;
    }

    private CellStyle numberStyle(Workbook wb){
        CellStyle style = wb.createCellStyle();
        DataFormat format = wb.createDataFormat();

        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setDataFormat(format.getFormat(NUMBER_FORMAT));

        return style;
    }

    private CellStyle dateStyle(Workbook wb){
        CellStyle style = wb.createCellStyle();
        DataFormat format = wb.createDataFormat();

        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setDataFormat(format.getFormat(DateUtils.DD_MMM_YYYY));

        return style;
    }

    private CellStyle headerStyle(Workbook wb){
        CellStyle style = borderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);

        return style;
    }

    private CellStyle textStyleBordered(Workbook wb){
        CellStyle style = borderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);

        return style;
    }

    private CellStyle numberStyleBordered(Workbook wb){
        CellStyle style = borderedStyle(wb);
        DataFormat format = wb.createDataFormat();

        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setDataFormat(format.getFormat(NUMBER_FORMAT));

        return style;
    }

    private CellStyle dateStyleBordered(Workbook wb){
        CellStyle style = borderedStyle(wb);
        DataFormat format = wb.createDataFormat();

        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setDataFormat(format.getFormat(DateUtils.DD_MMM_YYYY));

        return style;
    }


    private CellStyle borderedStyle(Workbook wb){
        Short thin = CellStyle.BORDER_THIN;
        short black = IndexedColors.BLACK.getIndex();

        CellStyle style = wb.createCellStyle();
        style.setBorderRight(thin);
        style.setRightBorderColor(black);
        style.setBorderBottom(thin);
        style.setBottomBorderColor(black);
        style.setBorderLeft(thin);
        style.setLeftBorderColor(black);
        style.setBorderTop(thin);
        style.setTopBorderColor(black);
        return style;
    }


}
