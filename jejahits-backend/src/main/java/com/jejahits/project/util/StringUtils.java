package com.jejahits.project.util;

import java.util.Arrays;
import java.util.List;


public class StringUtils {

    /**
     * Extract long string value to be List of String
     * ex : [ aaa, bbb ]
     * @return List<String>
     */
    public static List<String> extractStringToList(String value, String splitter){

        String[] arrays = value.split(splitter);

        return Arrays.asList(arrays);

    }

}
