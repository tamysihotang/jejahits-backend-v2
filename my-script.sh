#!/usr/bin/env bash

cd /opt/project;
sudo ./projectctl stop;
sudo rm -rf *.jar;
cd /home/jepta;
sudo cp jejahits_backend-1.0.0.RELEASE.jar /opt/project;
cd /opt/project;
sudo ./projectctl start;